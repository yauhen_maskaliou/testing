<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 5 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<fmt:setLocale value="${sessionScope.lang}" />
<fmt:setBundle basename="content" />
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="stylesheet" href="css/bootstrap.min.css" />
<title><fmt:message key="signin" /></title>

<script type="text/javascript">
	function check()
	{
		if (document.getElementById('inputlogin').value == '' || document.getElementById('inputpassword').value == '')
		{
			alert('Введите все данные');
			return false;
		}
		return true;
	}	
</script>

</head>
<body>
	<div class="container">

		<form class="form-signin" onsubmit="return check( );">
			<h2 class="form-signin-heading"><fmt:message key="signin" /></h2>
			<input type="text"  maxlength=20   id="inputlogin"
			class="input-block-level" placeholder="<fmt:message key="login" />" name="inputlogin">
			<input type="password" maxlength=20 id="inputpassword" 
			class="input-block-level" placeholder="<fmt:message key="password" />" name="inputpassword">
			<input type="hidden" name="command" value="login"> <br />
			<fmt:message key="language" />
			<select name="lang">
		  		<option selected value="ru">Русский</option>
		  		<option value="en">English</option>
	  		</select>
	  		<br />
			<button  class="btn btn-large btn-primary" type="submit" 
			 formaction="act"> <fmt:message key="signin" /> </button>
		</form>
		
		<form>
		<input type="hidden" name="command" value="showregisterform">
		<h3 class="form-signin-heading"><fmt:message key="notregistered" /></h3> 
		<button class="btn btn-large btn-primary" type="submit" formaction="act"> <fmt:message key="register" /> </button>
		</form>

	</div>
</body>
</html>
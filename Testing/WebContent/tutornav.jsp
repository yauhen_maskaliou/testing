<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<fmt:setLocale value="${sessionScope.lang}" />
<fmt:setBundle basename="content" />
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="stylesheet" href="resources/css/bootstrap.min.css" type="text/css" media="all" />
<title></title>
</head>
<body>
<nav class="navbar navbar-default">
  <div class="container-fluid">
	<form class="navbar-form navbar-left">
		<input type="hidden" name="command" value="showtopics">
        <button type="submit" class="btn btn-large btn-link" formaction="act"><fmt:message key="tests" /></button>
    </form>
    <form class="navbar-form navbar-left">
    	<input type="hidden" name="command" value="addtest">
        <button type="submit" class="btn btn-large btn-link" formaction="act"><fmt:message key="addtest" /></button>
     </form>
     <form class="navbar-form navbar-left">
    	<input type="hidden" name="command" value="logout">
        <button type="submit" class="btn btn-large btn-link" formaction="act"><fmt:message key="logout" /></button>
     </form>
  </div>
</nav>
</body>
</html>
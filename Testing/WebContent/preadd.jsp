<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<fmt:setLocale value="${sessionScope.lang}" />
<fmt:setBundle basename="content" />
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="stylesheet" href="css/bootstrap.min.css" />
<title><fmt:message key="adding" /></title>
<script type="text/javascript">
	function check()
	{
		if (document.getElementById('inputtitle').value == '')
		{
			alert('Введите все данные');
			return false;
		}
		return true;
	}	
</script>
</head>
<body>	
  	<jsp:include page="tutornav.jsp" />
  	
  	<form onsubmit="return check( );">
	<div class="panel panel-default">
	<div class="panel-heading"><fmt:message key="choose" /></div>
    <div class="panel-body">
        		
  	<select name="inputtopic">
  	<option disabled><fmt:message key="topic" /></option>
  	<c:set var="i" value="1"/>
	<c:forEach var="it" items="${requestScope.list}">
		<option value="${i}">${it}</option>
		<c:set var="i" value="${i + 1}"/>
	</c:forEach>
	</select>
  	<br /> <br />
  	
  	<select name="inputnumber" >
  	<option disabled><fmt:message key="numberofquestions" /></option>
  	<option selected value="5">5</option>
  	<c:forEach var="i" begin="6" end="10">
  		<option value="${i}">${i}</option>
  	</c:forEach>
  	</select>
	<br /> <br />
	
	<input type="text" maxlength=20 class="input-block-level" id="inputtitle"
	placeholder="<fmt:message key="title" />" name="inputtitle">
	
	</div>
    </div>
	<input type="hidden" name="command" value="showaddform">
	<button class="btn btn-large btn-primary" type="submit" formaction="act"><fmt:message key="begin" /> </button>
	
	</form>		
</body>
</html>
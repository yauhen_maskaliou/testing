<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<fmt:setLocale value="${sessionScope.lang}" />
<fmt:setBundle basename="content" />
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="stylesheet" href="css/bootstrap.min.css" />
<title></title>
</head>
<body>
	<h2> <fmt:message key="busylogin" /> </h2>
	<form>
		<input type="hidden" name="command" value="showregisterform">
		<button class="btn btn-large btn-primary" type="submit" formaction="act"> <fmt:message key="backtoreg" /> </button>
	</form>
</body>
</html>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<fmt:setLocale value="${sessionScope.lang}" />
<fmt:setBundle basename="content" />
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="stylesheet" href="resources/css/bootstrap.min.css" type="text/css" media="all" />
<title><fmt:message key="test" /></title>
</head>
<body>	
  	<jsp:include page="studentnav.jsp" />
	
	<form>
	<c:set var="i" value="1"/>
	<h1> <fmt:message key="testontopic" />  <font color="red"> <i> "${requestScope.list.title}" </i> </font> </h1>
	<c:forEach var="it" items="${requestScope.list.list}">
		<div class="panel panel-default">
			<div class="panel-heading"><h2> ${i}. ${it} </h2></div>
        	<div class="panel-body">
        		<c:set var="j" value="1"/>
        		<c:forEach var="it2" items="${it.answerList}">
					<input type="checkbox" name="q${i}a${j}"> ${it2} <br />
					<c:set var="j" value="${j + 1}" />
			    </c:forEach>
			    <br />
        	</div>
        </div>
        <c:set var="i" value="${i + 1}" />
	</c:forEach>
	<br /> <br />
	<input type="hidden" name="command" value="finishtest">
	<button class="btn btn-large btn-primary" type="submit" formaction="act"> <fmt:message key="finishtest" /> </button>
	</form>	
</body>
</html>
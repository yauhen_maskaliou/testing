<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<fmt:setLocale value="${sessionScope.lang}" />
<fmt:setBundle basename="content" />
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="stylesheet" href="css/bootstrap.min.css" />
<title><fmt:message key="registration" /></title>
<script type="text/javascript">
	function check()
	{
		if (document.getElementById('inputlogin').value == '' || document.getElementById('inputpassword').value == ''
				||document.getElementById('inputname').value == '' || document.getElementById('inputsurname').value == '')
		{
			alert('Введите все данные');
			return false;
		}
		return true;
	}	
</script>
</head>
<body>
	<div class="container">
		<form class="form-signin" onsubmit="return check( );">
			<h2 class="form-signin-heading"><fmt:message key="registration" /></h2>
			<input type="text" maxlength=20 class="input-block-level" id="inputlogin"
			placeholder="<fmt:message key="login" />" name="inputlogin">
			<input type="password" maxlength=20 class="input-block-level" id="inputpassword"
			placeholder="<fmt:message key="password" />" name="inputpassword">
			<input type="text" maxlength=20 class="input-block-level" id="inputname"
			placeholder="<fmt:message key="name" />" name="inputname">
			<input type="text" maxlength=20 class="input-block-level" id="inputsurname"
			placeholder="<fmt:message key="surname" />" name="inputsurname">
			<input type="hidden" name="command" value="register">
			<button class="btn btn-large btn-primary" type="submit" formaction="act"> <fmt:message key="register" /> </button>
		</form> 
	</div>
</body>
</html>
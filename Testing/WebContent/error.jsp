<%@ page isErrorPage="true" language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<fmt:setLocale value="${sessionScope.lang}" />
<fmt:setBundle basename="content" />
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="stylesheet" href="resources/css/bootstrap.min.css" type="text/css" media="all" />
<title><fmt:message key="error"/></title>
</head>
<body>

<div class="panel panel-default">
	<div class="panel-heading"><h2> <fmt:message key="error"/> </h2></div>
    <div class="panel-body">
		<c:if test="${not empty message}">
	       <div class="notice error" id="message">
	            <div id="message-content">
	             	${message}
	            </div>
	       </div>
           <br/><br/>
        </c:if>		
  	</div>
</div>

<form>
<input type="hidden" name="command" value="showstartpage">
<button type="submit" formaction="act" class="btn btn-large btn-primary"><fmt:message key="home"/></button>
</form>

</body>
</html>
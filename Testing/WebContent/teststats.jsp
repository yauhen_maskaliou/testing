<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<fmt:setLocale value="${sessionScope.lang}" />
<fmt:setBundle basename="content" />
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="stylesheet" href="resources/css/bootstrap.min.css" type="text/css" media="all" />
<title><fmt:message key="teststats" /></title>
</head>
<body>
	<jsp:include page="tutornav.jsp" />
	
	<fmt:message key="statsontest" /> "${requestScope.title}":
	<table class="table table-striped" border="3" cellpadding="3">
		<thead>
		<tr>
			<th><fmt:message key="login" /></th>
			<th><fmt:message key="name" /></th>
			<th><fmt:message key="surname" /></th>
			<th><fmt:message key="date" /></th>
			<th><fmt:message key="result" /></th>
		</tr>
		</thead>
		<c:forEach var="it" items="${requestScope.results}">
			<tr>
				<td>${it.student.login}</td>
				<td>${it.student.name}</td>
				<td>${it.student.surname}</td>
				<td>${it.date}</td>
				<td>${it.result}</td>
			</tr>
		</c:forEach>
	</table>
</body>
</html>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib uri="/WEB-INF/list.tld" prefix="list" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<fmt:setLocale value="${sessionScope.lang}" />
<fmt:setBundle basename="content" />
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="stylesheet" href="resources/css/bootstrap.min.css" type="text/css" media="all" />
<title><fmt:message key="topics" /></title>
</head>
<body>
	<jsp:include page="tutornav.jsp" />
	<form>
	<input type="hidden" name="command" value="showtestsontopic">
	<div class="panel panel-default">
	<div class="panel-heading">
	</div>
	<div class="panel-body">
	<h3> <fmt:message key="availabletopics" /> </h3>
	
	<list:mylist symbol="." first="1">
		<c:set var="i" value="1"/>
		<c:forEach var="it" items="${requestScope.list}">
			<list:myitem>
				<button type="submit" class="btn btn-large btn-link" name="topic" value="${i}" formaction="act">
				 	${it} 
				 </button>
			</list:myitem>
			<c:set var="i" value="${i + 1}"/>
		</c:forEach>
	</list:mylist>
	
	</div>
	</div>
	</form>
</body>
</html>
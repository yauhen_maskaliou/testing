<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib uri="/WEB-INF/list.tld" prefix="list" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<fmt:setLocale value="${sessionScope.lang}" />
<fmt:setBundle basename="content" />
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="stylesheet" href="resources/css/bootstrap.min.css" type="text/css" media="all" />
<title><fmt:message key="tests" /></title>
</head>
<body>
	<jsp:include page="studentnav.jsp" />
	<form>
	<input type="hidden" name="command" value="showtest">
	<h1> <fmt:message key="testsontopic" />  <font color="red"> <i> "${requestScope.topic}" </i> </font> </h1>
	
	<list:mylist symbol="." first="1">
		<c:forEach var="it" items="${requestScope.list}">
		<list:myitem>
		<button type="submit" class="btn btn-large btn-link" name="testid" value="${it.id}" formaction="act">
		 	${it.title} 
		 </button>
		 </list:myitem>
	</c:forEach>
	</list:mylist>

	</form>
</body>
</html>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<fmt:setLocale value="${sessionScope.lang}" />
<fmt:setBundle basename="content" />
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="stylesheet" href="resources/css/bootstrap.min.css" type="text/css" media="all" />
<title><fmt:message key="adding" /></title>
<script type="text/javascript">
	function check()
	{
		if (document.getElementById('inputlogin').value == '')
		{
			alert('Введите все данные');
			return false;
		}
		return true;
	}	
</script>
</head>
<body>	
  	<jsp:include page="tutornav.jsp" />
	
	<form>
	<h1><fmt:message key="testcreating" /></h1>
	<h3><fmt:message key="enterquestions" /></h3>
	<c:forEach var="i" begin="1" end="${sessionScope.inputnumber}">
  		<div class="panel panel-default">
			<div class="panel-heading">${i}. <input type="text"  id="q${i}"
			maxlength=100 class="input-block-level" name="q${i}"></div>
        	<div class="panel-body">
        		<c:forEach var="j" begin="1" end="5">
					<input type="checkbox" name="q${i}ch${j}"> 
					<input type="text" maxlength=100 id="q${i}a${j}"
					class="input-block-level" name="q${i}a${j}"> <br />
			    </c:forEach>
			    <br />
        	</div>
        </div>
  	</c:forEach>
	
	<br /> <br />
	<input type="hidden" name="command" value="finishadding">
	<button class="btn btn-large btn-primary" type="submit" formaction="act"> <fmt:message key="add" /> </button>
	</form>	
</body>
</html>
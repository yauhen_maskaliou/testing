<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<fmt:setLocale value="${sessionScope.lang}" />
<fmt:setBundle basename="content" />
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="stylesheet" href="resources/css/bootstrap.min.css" type="text/css" media="all" />
<title><fmt:message key="test" /></title>
</head>
<body>	
  	<jsp:include page="tutornav.jsp" />
	<h1> <fmt:message key="testontopic" />  <font color="red"> <i> "${requestScope.list.title}" </i> </font> </h1>
	
	<form>
	<input type="hidden" name="command" value="showteststats">
	<button class="btn btn-large btn-primary" type="submit" formaction="act"> <fmt:message key="teststats" /> </button>
	</form>
	
	<form>
	<input type="hidden" name="command" value="edittest">
	<button class="btn btn-large btn-primary" type="submit" formaction="act"> <fmt:message key="edittest" /> </button>
	</form>	
	
	<form>
	<input type="hidden" name="command" value="deletetest">
	<button class="btn btn-large btn-primary" type="submit" formaction="act"> <fmt:message key="deletetest" /> </button>
	</form>		
</body>
</html>
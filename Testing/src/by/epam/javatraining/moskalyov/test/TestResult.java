package by.epam.javatraining.moskalyov.test;

import by.epam.javatraining.moskalyov.user.Student;

public class TestResult
{
    private Student student;
    private Test test;
    private String result;
    private String date;
    
    public TestResult(Student student, Test test, String result, String date)
    {
	super();
	this.student = student;
	this.test = test;
	this.result = result;
	this.date = date;
    }

    public Student getStudent()
    {
        return student;
    }

    public void setStudent(Student student)
    {
        this.student = student;
    }

    public Test getTest()
    {
        return test;
    }

    public void setTest(Test test)
    {
        this.test = test;
    }

    public String getResult()
    {
        return result;
    }

    public void setResult(String result)
    {

    }

    public String getDate()
    {
        return date;
    }

    public void setDate(String date)
    {
        this.date = date;
    }
}

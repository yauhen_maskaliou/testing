package by.epam.javatraining.moskalyov.test;

import java.io.Serializable;

public class Answer implements Serializable
{
    private int id;
    private String answerText;
    private boolean right;
    
    public Answer(int id, String answerText, boolean right)
    {
	super();
	this.id = id;
	this.answerText = answerText;
	this.right = right;
    }
    
    public Answer(String answerText, boolean right)
    {
	super();
	this.id = 0;
	this.answerText = answerText;
	this.right = right;
    }

    public int getId()
    {
        return id;
    }

    public void setId(int id)
    {
        this.id = id;
    }

    public String getAnswerText()
    {
        return answerText;
    }

    public void setAnswerText(String answerText)
    {
        this.answerText = answerText;
    }

    public boolean isRight()
    {
        return right;
    }

    public void setRight(boolean right)
    {
        this.right = right;
    }
    
    public String toString()
    {
	return answerText;
    }
}

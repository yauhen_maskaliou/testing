package by.epam.javatraining.moskalyov.test;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class Question implements Serializable
{
    private int id;
    private String questionText;
    private List<Answer> answerList;
    
    public Question(int id, String questionText, List<Answer> answerList)
    {
	super();
	this.id = id;
	this.questionText = questionText;
	this.answerList = answerList;
    }
    
    public Question(int id, String questionText)
    {
	super();
	this.id = id;
	this.questionText = questionText;
	this.answerList = new ArrayList<Answer>();
    }
    
    public Question(String questionText)
    {
	super();
	this.id = 0;
	this.questionText = questionText;
	this.answerList = new ArrayList<Answer>();
    }

    public int getId()
    {
        return id;
    }

    public void setId(int id)
    {
        this.id = id;
    }

    public String getQuestionText()
    {
        return questionText;
    }

    public void setQuestionText(String questionText)
    {
        this.questionText = questionText;
    }

    public List<Answer> getAnswerList()
    {
        return answerList;
    }

    public void setAnswerList(List<Answer> answerList)
    {
        this.answerList = answerList;
    }

    public String toString()
    {
	return questionText;
    }
}

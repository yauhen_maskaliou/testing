package by.epam.javatraining.moskalyov.test;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class Test implements Iterable<Question>, Serializable
{
    private int id;
    private List<Question> list;
    private String topic;
    private String title;
    
    public Test(int id, String topic, String title)
    {
	this.topic = topic;
	this.id = id;
	this.title = title;
	this.list = new ArrayList<Question>();
    }

    public Test(int id, String topic, String title, List<Question> list)
    {
	super();
	this.id = id;
	this.title = title;
	this.list = list;
	this.topic = topic;
    }
    
    public List<Question> getList()
    {
        return list;
    }

    public void setList(List<Question> list)
    {
        this.list = list;
    }

    public String getTopic()
    {
        return topic;
    }

    public void setTopic(String topic)
    {
        this.topic = topic;
    }

    public int getId()
    {
        return id;
    }

    public void setId(int id)
    {
        this.id = id;
    }

    public String getTitle()
    {
        return title;
    }

    public void setTitle(String title)
    {
        this.title = title;
    }

    public void add(Question question)
    {
	list.add(question);
    }
    
    public void remove(int i)
    {
	list.remove(i);
    }
    
    @Override
    public Iterator<Question> iterator()
    {
	return list.iterator();
    }
}

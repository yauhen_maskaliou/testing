package by.epam.javatraining.moskalyov.controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import by.epam.javatraining.moskalyov.command.CommandFactory;
import by.epam.javatraining.moskalyov.connection.ConnectionPool;
import by.epam.javatraining.moskalyov.exception.LogicException;

/**
 * This {@link HttpServlet Servlet} represents Controller in MVC architecture.
 */
@WebServlet("/act")
public class Controller extends HttpServlet
{
    private static final long serialVersionUID = 1L;
    
    public static final Logger LOG = Logger.getRootLogger();

    public Controller()
    {
	
    }

    protected void doGet(HttpServletRequest request,
	    HttpServletResponse response) throws ServletException, IOException
    {	
	processRequest(request, response);
    }

    protected void doPost(HttpServletRequest request,
	    HttpServletResponse response) throws ServletException, IOException
    {
	processRequest(request, response);
    }
    
    /**
     * Initializes controller: initializes connection pool.
     *
     * @throws ServletException if any servlet problems occur
     */
    @Override
    public void init() throws ServletException
    {
            ConnectionPool.getInstance();
    }

    private void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        try
        {
            getServletContext().getRequestDispatcher(CommandFactory.defineCommand(request).execute(request)).forward(request, response);
        }
        catch (LogicException e)
        {
            LOG.error(e);
            request.setAttribute("message", e.getMessage());
            getServletContext().getRequestDispatcher("/error.jsp").forward(request, response);
        }
    }

    /**
     * Releases connection pool.
     */
    @Override
    public void destroy()
    {
        ConnectionPool.getInstance().releasePool();
    }
}

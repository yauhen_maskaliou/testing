package by.epam.javatraining.moskalyov.tag;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspTagException;
import javax.servlet.jsp.tagext.BodyTagSupport;
import java.io.IOException;

public class ItemTag extends BodyTagSupport
{
    private static final long serialVersionUID = 1L;
    private String style;

    public void setStyle(String style)
    {
	this.style = style;
    }

    public int doEndTag() throws JspException, JspTagException
    {
	if (style == null)
	    style = "";
	ListTag parent = (ListTag) findAncestorWithClass(this, ListTag.class);
	if (parent == null)
	    try
	    {
		pageContext.getOut().print(
			"<span style=\"" + style + "\">"
				+ getBodyContent().getString() + "</span><br>");
	    }
	    catch (IOException ioException)
	    {
		throw new JspException("Error: " + ioException.getMessage());
	    }
	    else
	    try
	    {
		pageContext.getOut().print(
			parent.getNumber() + "<span style=\"" + style + "\">"
				+ getBodyContent().getString() + "</span><br>");
	    }
	    catch (IOException ioException)
	    {
		throw new JspException("Error: " + ioException.getMessage());
	    }
	return SKIP_BODY;
    }
}

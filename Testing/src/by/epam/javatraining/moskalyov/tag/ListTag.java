package by.epam.javatraining.moskalyov.tag;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.TagSupport;

public class ListTag extends TagSupport
{
    private static final long serialVersionUID = 1L;

    private String first;
    private String symbol;
    private Integer current = 1;

    public void setFirst(String first)
    {
	this.first = first;
    }

    public void setSymbol(String symbol)
    {
	this.symbol = symbol;
    }


    @Override
    public int doStartTag()
    {
	if (symbol == null)
	    symbol = ".";
	try
	{
	    current = Integer.valueOf(first);
	    if (current < 1)
		throw new NumberFormatException();
	}
	catch (Exception e)
	{
	    current = 1;
	}
	return EVAL_BODY_INCLUDE;
    }

    @Override
    public int doEndTag() throws JspException
    {
	return SKIP_BODY;
    }

    public String getNumber()
    {
	String ans = getNumber(current) + symbol + " ";
	current += 1;
	return ans;
    }

    private String getNumber(int i)
    {
	return String.valueOf(i);
    }
}

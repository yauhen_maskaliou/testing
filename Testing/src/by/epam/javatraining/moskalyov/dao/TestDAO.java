package by.epam.javatraining.moskalyov.dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import by.epam.javatraining.moskalyov.exception.DAOException;
import by.epam.javatraining.moskalyov.exception.LogicException;
import by.epam.javatraining.moskalyov.exception.TechnicalException;
import by.epam.javatraining.moskalyov.logic.UserLogic;
import by.epam.javatraining.moskalyov.test.Answer;
import by.epam.javatraining.moskalyov.test.Question;
import by.epam.javatraining.moskalyov.test.Test;
import by.epam.javatraining.moskalyov.test.TestResult;

/**
 * This class makes all operations with tests in database.
 */
public class TestDAO extends DAO
{
    /**
     * Creates DAO and tries to access a free connection from {@link ConnectionPool}.
     *
     * @throws TechnicalException if couldn't get a free connection
     */
    public TestDAO() throws TechnicalException
    {
	
    }
    
    /**
     * Returns test with required id.
     *
     * @param id
     * @return test by id
     * @throws DAOException if any problems with accessing objects from database occur
     */
    public String getTestTitleById(int id) throws DAOException
    {
	try (PreparedStatement ps = connection.prepareStatement("SELECT * FROM tests WHERE id=?"))
        {
            ps.setInt(1, id);
            ResultSet rs = ps.executeQuery();
            rs.next();
	    return rs.getString("title");
        }
        catch (SQLException e)
        {
            throw new DAOException("Error in getTestTitleById", e);
        }    
    }
    
    /**
     * Returns topic with required id.
     *
     * @param id
     * @return topic by id
     * @throws DAOException if any problems with accessing objects from database occur
     */
    public String getTopicById(int id) throws DAOException
    {
	try (PreparedStatement ps = connection.prepareStatement("SELECT * FROM topics WHERE id=?"))
        {
            ps.setInt(1, id);
            ResultSet rs = ps.executeQuery();
            rs.next();
	    return rs.getString("name");
        }
        catch (SQLException e)
        {
            throw new DAOException("Error in getTopicById", e);
        }    
    }
    
    /**
     * Returns test topic with required test id.
     *
     * @param test id
     * @return test topic
     * @throws DAOException if any problems with accessing objects from database occur
     */
    public int getTestTopicByTestId(int id) throws DAOException
    {
	try (PreparedStatement ps = connection.prepareStatement("SELECT * FROM tests WHERE id=?"))
        {
            ps.setInt(1, id);
            ResultSet rs = ps.executeQuery();
            rs.next();
	    return rs.getInt("topic");
        }
        catch (SQLException e)
        {
            throw new DAOException("Error in getTestTopicById", e);
        }    
    }
    
    /**
     * Returns list of answers in question.
     *
     * @param questions
     * @return list of answers
     * @throws DAOException if any problems with accessing objects from database occur
     */
    public List<Answer> getAnswersByQuestion(Question question) throws DAOException
    {
	try (PreparedStatement ps = connection.prepareStatement("SELECT * FROM answers WHERE question=?"))
        {
            ps.setInt(1, question.getId());
	    ResultSet rs = ps.executeQuery();
	    List<Answer> a = new ArrayList<Answer>();
	    while (rs.next())
	    {
		a.add(new Answer(rs.getInt("id"), rs.getString("text"), rs.getBoolean("right")));
	    }
	    return a;
	}
        catch (SQLException e)
        {
            throw new DAOException("Error in getAnswersByQuestion", e);
        }    
    }
    
    /**
     * Returns list of questions from test.
     *
     * @param test id
     * @return list of questions
     * @throws DAOException if any problems with accessing objects from database occur
     */
    public List<Question> getQuestionsByTestId(int id) throws DAOException
    {
	try (PreparedStatement ps = connection.prepareStatement("SELECT * FROM questions WHERE test=?"))
        {
            ps.setInt(1, id);
            ResultSet rs = ps.executeQuery();
            List<Question> q = new ArrayList<Question>();
	    while (rs.next())
	    {
		q.add(new Question(rs.getInt("id"), rs.getString("text")));
	    }
	    
	    List<Question> questionList = new ArrayList<Question>();
	    for(Question t : q)
	    {
		t.setAnswerList(getAnswersByQuestion(t));
		questionList.add(t);
	    }
	    return questionList;
        }
        catch (SQLException e)
        {
            throw new DAOException("Error in getQuestionsByTestId", e);
        } 
    }
    
    /**
     * Returns test by id.
     *
     * @param test id
     * @return test
     * @throws DAOException if any problems with accessing objects from database occur
     */
    public Test getTestById(int id) throws DAOException
    {
	return new Test(id, getTopicById(getTestTopicByTestId(id)), getTestTitleById(id), getQuestionsByTestId(id));
    }
    
    /**
     * Returns all available topics.
     *
     * @return all topics
     * @throws DAOException if any problems with accessing objects from database occur
     */
    public List<String> getAllTopics() throws DAOException
    {
	try (PreparedStatement ps = connection.prepareStatement("SELECT * FROM topics"))
        {
            ResultSet rs = ps.executeQuery();
            List<String> topics = new ArrayList<String>();
	    while (rs.next())
	    {
		topics.add(rs.getString("name"));
	    }
	    
	    return topics;
        }
        catch (SQLException e)
        {
            throw new DAOException("Error in getAllTopics", e);
        } 
    }
    
    /**
     * Returns list of tests by topic id.
     *
     * @param topic id
     * @return list of tests
     * @throws DAOException if any problems with accessing objects from database occur
     */
    public List<Test> getTestsByTopic(int id) throws DAOException
    {
	try (PreparedStatement ps = connection.prepareStatement("SELECT * FROM tests WHERE topic=?"))
        {
	    ps.setInt(1, id);
            ResultSet rs = ps.executeQuery();
            List<Test> tests = new ArrayList<Test>();
	    while (rs.next())
	    {
		tests.add(new Test(rs.getInt("id"), rs.getString("topic"), rs.getString("title")));
	    }
	    
	    return tests;
        }
        catch (SQLException e)
        {
            throw new DAOException("Error in getTestsByTopic", e);
        } 
    }
    
    /**
     * Adds new result to database.
     *
     * @param test id, student id, test result
     * @throws DAOException if any problems with accessing objects from database occur
     */
    public void addNewResult(int testid, int studentid, String result) throws DAOException
    {
	try (PreparedStatement ps = connection.prepareStatement("INSERT INTO results (test, student, result, date) VALUES (?, ?, ?, ?)"))
        {
            ps.setInt(1, testid);
            ps.setInt(2, studentid);
            ps.setString(3, result);
            Calendar c = Calendar.getInstance();
    	    ps.setString(4, c.get(Calendar.DAY_OF_MONTH) + "." + (c.get(Calendar.MONTH) + 1) + "." + c.get(Calendar.YEAR)
    		+ " " + c.get(Calendar.HOUR) + ":" + c.get(Calendar.MINUTE));
	    ps.executeUpdate();
	}
        catch (SQLException e)
        {
            System.out.println(e);
            throw new DAOException("Error in addNewResult", e);
        } 
    }
    
    /**
     * Returns list of all student reults.
     *
     * @param student id
     * @return list of results
     * @throws DAOException if any problems with accessing objects from database occur
     */
    public List<TestResult> getStudentResults(int id) throws DAOException, LogicException
    {
	try (PreparedStatement ps = connection.prepareStatement("SELECT * FROM results WHERE student=?"))
        {
            ps.setInt(1, id);
	    ResultSet rs = ps.executeQuery();
	    
	    List<TestResult> results = new ArrayList<TestResult>();  
	    while (rs.next())
	    {
		results.add(new TestResult(UserLogic.getStudent(rs.getInt("student")), 
			getTestById(rs.getInt("test")), rs.getString("result"), rs.getString("date")));
	    }
	    return results;
	}
        catch (SQLException e)
        {
            System.out.println(e);
            throw new DAOException("Error in getStudentResults", e);
        } 
    }
    
    /**
     * Returns list with all result on test
     *      
     * @param test id
     * @return list of results
     * @throws DAOException if any problems with accessing objects from database occur
     */
    public List<TestResult> getTestStats(int id) throws DAOException, LogicException
    {
	try (PreparedStatement ps = connection.prepareStatement("SELECT * FROM results WHERE test=?"))
        {
            ps.setInt(1, id);
	    ResultSet rs = ps.executeQuery();
	    
	    List<TestResult> results = new ArrayList<TestResult>();  
	    while (rs.next())
	    {
		results.add(new TestResult(UserLogic.getStudent(rs.getInt("student")), 
			getTestById(rs.getInt("test")), rs.getString("result"), rs.getString("date")));
	    }
	    return results;
	}
        catch (SQLException e)
        {
            System.out.println(e);
            throw new DAOException("Error in getTestStats()", e);
        } 
    }
    
    /**
     * Adds new test to database using information from request parameters.
     *
     * @param request
     * @throws DAOException if any problems with accessing objects from database occur
     */
    public void addNewTest(HttpServletRequest request) throws DAOException
    {
	try (PreparedStatement ps = connection.prepareStatement("INSERT INTO tests (topic, title, tutor) VALUES (?, ?, ?)"))
        {
            ps.setInt(1, Integer.parseInt((String) request.getSession().getAttribute("inputtopic")));
            ps.setString(2, (String) request.getSession().getAttribute("inputtitle"));
            ps.setInt(3, (int) request.getSession().getAttribute("id"));
	    ps.executeUpdate();
	}
        catch (SQLException e)
        {
            System.out.println(e);
            throw new DAOException("Error in addNewTest()", e);
        } 
    }
    
    /**
     * Returns id of last added test.
     *
     * @return id of last added test
     * @throws DAOException if any problems with accessing objects from database occur
     */
    public int getLastTestId() throws DAOException
    {
	try (PreparedStatement ps = connection.prepareStatement("SELECT * FROM tests"))
        {
	    ResultSet rs = ps.executeQuery();
	     
	    int id = 0;
	    while (rs.next())
	    {
		id = rs.getInt("id");
	    }
	    return id;
	}
        catch (SQLException e)
        {
            System.out.println(e);
            throw new DAOException("Error in getLastTestId()", e);
        } 
    }
    
    /**
     * Adds new question to database.
     *      
     * @param question
     * @throws DAOException if any problems with accessing objects from database occur
     */
    public void addNewQuestion(Question question) throws DAOException
    {
	try (PreparedStatement ps = connection.prepareStatement("INSERT INTO questions (text, test) VALUES (?, ?)"))
        {
	    ps.setString(1, question.getQuestionText());
	    ps.setInt(2, getLastTestId());
	    ps.executeUpdate();
	}
        catch (SQLException e)
        {
            System.out.println(e);
            throw new DAOException("Error in addNewQuestion()", e);
        }
    }
    
    /**
     * Returns id of last added question.
     *      
     * @return id of last added question.
     * @throws DAOException if any problems with accessing objects from database occur
     */
    public int getLastQuestionId() throws DAOException
    {
	try (PreparedStatement ps = connection.prepareStatement("SELECT * FROM questions"))
        {
	    ResultSet rs = ps.executeQuery();
	     
	    int id = 0;
	    while (rs.next())
	    {
		id = rs.getInt("id");
	    }
	    return id;
	}
        catch (SQLException e)
        {
            System.out.println(e);
            throw new DAOException("Error in getLastQuestionId()", e);
        } 
    }
    
    /**
     * Adds new question to database.
     *      
     * @param answer
     * @throws DAOException if any problems with accessing objects from database occur
     */
    public void addNewAnswer(Answer answer) throws DAOException
    {
	try (PreparedStatement ps = connection.prepareStatement("INSERT INTO answers (text, question, `right`) VALUES (?, ?, ?)"))
        {
	    ps.setString(1, answer.getAnswerText());
	    ps.setInt(2, getLastQuestionId());
	    ps.setBoolean(3, answer.isRight());
	    ps.executeUpdate();
	}
        catch (SQLException e)
        {
            System.out.println(e);
            throw new DAOException("Error in addNewAnswer()", e);
        }
    }
    
    /**
     * Updates question text in databse
     *      
     * @param question id, test
     * @throws DAOException if any problems with accessing objects from database occur
     */
    public void updateQuestion(int id, String text) throws DAOException
    {
	try (PreparedStatement ps = connection.prepareStatement("UPDATE questions SET text=? WHERE id=?"))
        {
	    ps.setString(1, text);
	    ps.setInt(2, id);
	    ps.executeUpdate();
	}
        catch (SQLException e)
        {
            System.out.println(e);
            throw new DAOException("Error in updateQuestion()", e);
        }
    }
    
    /**
     * Updates answer text in database.
     *      
     * @param answer id, text
     * @throws DAOException if any problems with accessing objects from database occur
     */
    public void updateAnswer(int id, String text, boolean right) throws DAOException
    {
	try (PreparedStatement ps = connection.prepareStatement("UPDATE answers SET text=?, `right`=? WHERE id=?"))
        {
	    ps.setString(1, text);
	    ps.setBoolean(2, right);
	    ps.setInt(3, id);
	    ps.executeUpdate();
	}
        catch (SQLException e)
        {
            System.out.println(e);
            throw new DAOException("Error in updateAnswer()", e);
        }
    }
    
    /**
     * Deletes all answers on question
     *      
     * @param question id
     * @throws DAOException if any problems with accessing objects from database occur
     */
    public void deleteAnswers(int questionid) throws DAOException
    {
	try (PreparedStatement ps = connection.prepareStatement("DELETE FROM answers WHERE question=?"))
        {
	    ps.setInt(1, questionid);
	    ps.executeUpdate();
	}
        catch (SQLException e)
        {
            System.out.println(e);
            throw new DAOException("Error in deleteAnswers()", e);
        }
    }
    
    /**
     * Deletes all questions on test.
     *      
     * @param test id
     * @throws DAOException if any problems with accessing objects from database occur
     */
    public void deleteQuestions(int testid) throws DAOException
    {
	try (PreparedStatement ps = connection.prepareStatement("DELETE FROM questions WHERE test=?"))
        {
	    ps.setInt(1, testid);
	    ps.executeUpdate();
	}
        catch (SQLException e)
        {
            System.out.println(e);
            throw new DAOException("Error in deleteQuestions()", e);
        }
    }
    
    /**
     * Deletes all results on test
     *      
     * @param test id
     * @throws DAOException if any problems with accessing objects from database occur
     */
    public void deleteResults(int testid) throws DAOException
    {
	try (PreparedStatement ps = connection.prepareStatement("DELETE FROM results WHERE test=?"))
        {
	    ps.setInt(1, testid);
	    ps.executeUpdate();
	}
        catch (SQLException e)
        {
            System.out.println(e);
            throw new DAOException("Error in deleteResults()", e);
        }
    }
    
    /**
     * Deletes test from database.
     *      
     * @param test id
     * @throws DAOException if any problems with accessing objects from database occur
     */
    public void deleteTest(int testid) throws DAOException
    {
	try (PreparedStatement ps = connection.prepareStatement("DELETE FROM tests WHERE id=?"))
        {
	    ps.setInt(1, testid);
	    ps.executeUpdate();
	}
        catch (SQLException e)
        {
            System.out.println(e);
            throw new DAOException("Error in deleteTest()", e);
        }
    }
}

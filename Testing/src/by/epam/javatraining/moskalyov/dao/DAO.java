package by.epam.javatraining.moskalyov.dao;

import java.sql.Connection;

import by.epam.javatraining.moskalyov.connection.ConnectionPool;
import by.epam.javatraining.moskalyov.exception.TechnicalException;

/**
 * This class is used for getting, inserting and updating entities from database.
 * After using any DAO, close it by calling appropriate method.
 */
public abstract class DAO  implements AutoCloseable
{
    protected Connection connection;
    
    private static final int WAIT_TIME_SEC = 10;

    public DAO() throws TechnicalException
    {
        connection = ConnectionPool.getInstance().getConnection(WAIT_TIME_SEC);
    }

    /**
     * Releases taken {@link Connection} back to pool.
     * This method is invoked automatically on objects managed by the
     * {@code try}-with-resources statement.
     */
    @Override
    public void close()
    {
        ConnectionPool.getInstance().releaseConnection(connection);
    }
}

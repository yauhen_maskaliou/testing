package by.epam.javatraining.moskalyov.dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import by.epam.javatraining.moskalyov.exception.DAOException;
import by.epam.javatraining.moskalyov.exception.TechnicalException;
import by.epam.javatraining.moskalyov.user.Student;

/**
 * This class makes all operations with users in database.
 */
public class UserDAO extends DAO
{
    /**
     * Creates DAO and tries to access a free connection from {@link ConnectionPool}.
     *
     * @throws TechnicalException if couldn't get a free connection
     */
    public UserDAO() throws TechnicalException
    {
	
    }

    /**
     * Returns student id, if he exists. Otherwise, returns 0.
     *
     * @param login, password
     * @return student id, if he exists. Otherwise, returns 0.
     * @throws DAOException if any problems with accessing objects from database occur
     */
    public int studentExists(String login, String password) throws DAOException
    {
	try (PreparedStatement ps = connection.prepareStatement("SELECT * FROM students WHERE login=? and password=?"))
        {
            ps.setString(1, login);
            ps.setString(2, password);
	    ResultSet rs = ps.executeQuery();
	    if(!rs.next())
	    {
		return 0;
	    }
	    else
	    {
		return rs.getInt("id");
	    }
	}
        catch (SQLException e)
        {
            throw new DAOException("Error in studentExists()", e);
        } 
    }
    
    /**
     * Returns tutor id, if he exists. Otherwise, returns 0.
     *
     * @param login, password
     * @return tutor id, if he exists. Otherwise, returns 0.
     * @throws DAOException if any problems with accessing objects from database occur
     */
    public int tutorExists(String login, String password) throws DAOException
    {
	try (PreparedStatement ps = connection.prepareStatement("SELECT * FROM tutors WHERE login=? and password=?"))
        {
            ps.setString(1, login);
            ps.setString(2, password);
	    ResultSet rs = ps.executeQuery();
	    if(!rs.next())
	    {
		return 0;
	    }
	    else
	    {
		return rs.getInt("id");
	    }
	}
        catch (SQLException e)
        {
            throw new DAOException("Error in tutorExists()", e);
        } 
    }
    
    /**
     * Checks if login is free or not.
     *
     * @param login
     * @return true, if free, false if not.
     * @throws DAOException if any problems with accessing objects from database occur
     */
    public boolean isLoginFree(String login) throws DAOException
    {
	try (PreparedStatement ps = connection.prepareStatement("SELECT * FROM students WHERE login=?"))
        {
            ps.setString(1, login);
	    ResultSet rs = ps.executeQuery();
	    if(!rs.next())
	    {
		return true;
	    }
	    return false;
	}
        catch (SQLException e)
        {
            throw new DAOException("Error in isLoginFree()", e);
        } 
    }
    
    /**
     * Registers new user.
     *
     * @param login, password, name, surname
     * @throws DAOException if any problems with accessing objects from database occur
     */
    public void registerNewUser(String login, String password, String name, String surname) throws DAOException
    {
	//INSERT INTO students (name, surname, login, password) VALUES ('Jack', 'Shephard', 'doc', '123')
	try (PreparedStatement ps = connection.prepareStatement("INSERT INTO students (name, surname, login, password) VALUES (?, ?, ?, ?)"))
        {
            ps.setString(1, name);
            ps.setString(2, surname);
            ps.setString(3, login);
            ps.setString(4, password);
	    ps.executeUpdate();
	}
        catch (SQLException e)
        {
            System.out.println(e);
            throw new DAOException("Error in registerNewUser()", e);
        } 
    }
    
    /**
     * Returns student by id
     *
     * @param student id
     * @return student
     * @throws DAOException if any problems with accessing objects from database occur
     */  
    public Student getStudent(int id) throws DAOException
    {
	try (PreparedStatement ps = connection.prepareStatement("SELECT * FROM students WHERE id=?"))
        {
            ps.setInt(1, id);
	    ResultSet rs = ps.executeQuery();
	    rs.next();

	    return new Student(rs.getString("login"), rs.getString("password"), rs.getString("name"), rs.getString("surname"));
	}
        catch (SQLException e)
        {
            throw new DAOException("Error in getStudent()", e);
        } 
    }
}

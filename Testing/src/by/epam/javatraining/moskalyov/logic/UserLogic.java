package by.epam.javatraining.moskalyov.logic;

import by.epam.javatraining.moskalyov.dao.UserDAO;
import by.epam.javatraining.moskalyov.exception.DAOException;
import by.epam.javatraining.moskalyov.exception.LogicException;
import by.epam.javatraining.moskalyov.exception.TechnicalException;
import by.epam.javatraining.moskalyov.user.Student;

/**
 * All actions with users.
 */
public class UserLogic
{
    public static int studentExists(String login, String password) throws LogicException
    {
	try(UserDAO dao = new UserDAO())
	{
	    return dao.studentExists(login, password);
	}
	catch (TechnicalException | DAOException e)
        {
            throw new LogicException(e);
        }
    }
    
    public static int tutorExists(String login, String password) throws LogicException
    {
	try(UserDAO dao = new UserDAO())
	{
	    return dao.tutorExists(login, password);
	}
	catch (TechnicalException | DAOException e)
        {
            throw new LogicException(e);
        }
    }
    
    public static boolean isLoginFree(String login) throws LogicException
    {
	try(UserDAO dao = new UserDAO())
	{
	    return dao.isLoginFree(login);
	}
	catch (TechnicalException | DAOException e)
        {
            throw new LogicException(e);
        }
    }
    
    public static void registerNewUser(String login, String password, String name, String surname) throws LogicException
    {
	try(UserDAO dao = new UserDAO())
	{
	    dao.registerNewUser(login, password, name, surname);
	}
	catch (TechnicalException | DAOException e)
        {
            throw new LogicException(e);
        }
    }
    
    public static Student getStudent(int id) throws LogicException
    {
	try(UserDAO dao = new UserDAO())
	{
	    return dao.getStudent(id);
	}
	catch (TechnicalException | DAOException e)
        {
            throw new LogicException(e);
        }
    }
}

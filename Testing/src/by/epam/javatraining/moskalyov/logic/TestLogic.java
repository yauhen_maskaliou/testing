package by.epam.javatraining.moskalyov.logic;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import by.epam.javatraining.moskalyov.dao.TestDAO;
import by.epam.javatraining.moskalyov.exception.DAOException;
import by.epam.javatraining.moskalyov.exception.LogicException;
import by.epam.javatraining.moskalyov.exception.TechnicalException;
import by.epam.javatraining.moskalyov.test.Answer;
import by.epam.javatraining.moskalyov.test.Question;
import by.epam.javatraining.moskalyov.test.Test;
import by.epam.javatraining.moskalyov.test.TestResult;

/**
 * All actions with tests.
 */
public class TestLogic
{
    public static Test getTest(int id) throws LogicException
    {
	try(TestDAO dao = new TestDAO())
	{
	    return dao.getTestById(id);
	}
	catch (TechnicalException | DAOException e)
        {
            throw new LogicException(e);
        }
    }
    
    public static List<String> getAllTopics() throws LogicException
    {
	try(TestDAO dao = new TestDAO())
	{
	    return dao.getAllTopics();
	}
	catch (TechnicalException | DAOException e)
        {
            throw new LogicException(e);
        }
    }
    
    public static List<Test> getTestsByTopic(int id) throws LogicException
    {
	try(TestDAO dao = new TestDAO())
	{
	    return dao.getTestsByTopic(id);
	}
	catch (TechnicalException | DAOException e)
        {
            throw new LogicException(e);
        }
    }
    
    public static String getTopicById(int id) throws LogicException
    {
	try(TestDAO dao = new TestDAO())
	{
	    return dao.getTopicById(id);
	}
	catch (TechnicalException | DAOException e)
        {
            throw new LogicException(e);
        }
    }
    
    public static String checkResults(Test test, HttpServletRequest request)
    {
	int rightAnswers = 0;
	List<Question> questionList = test.getList();
	for(int i = 0; i < questionList.size(); i++)
	{
	    List<Answer> answerList = questionList.get(i).getAnswerList();
	    boolean isAllRight = true;
	    for(int j = 0; j < answerList.size(); j++)
	    {
		if((request.getParameter("q"+String.valueOf(i+1) + "a" + String.valueOf(j+1)) != null
			&& !answerList.get(j).isRight()) 
			|| (request.getParameter("q"+String.valueOf(i+1) + "a" + String.valueOf(j+1)) == null)
			&& answerList.get(j).isRight())
		{
		    isAllRight = false;
		    break;
		}
	    }
	    if(isAllRight)
	    {
		rightAnswers++;
	    }
	}
	String result = String.valueOf(rightAnswers) + "/" + String.valueOf(questionList.size());
	return result;
    }
    
    public static void addNewResult(int testid, int studentid, String result) throws LogicException
    {
	try(TestDAO dao = new TestDAO())
	{
	    dao.addNewResult(testid, studentid, result);
	}
	catch (TechnicalException | DAOException e)
        {
            throw new LogicException(e);
        }
    }
    
    public static List<TestResult> getStudentResults(int id) throws LogicException
    {
	try(TestDAO dao = new TestDAO())
	{
	    return dao.getStudentResults(id);
	}
	catch (TechnicalException | DAOException e)
        {
            throw new LogicException(e);
        }
    }
    
    public static List<TestResult> getTestStats(int id) throws LogicException
    {
	try(TestDAO dao = new TestDAO())
	{
	    return dao.getTestStats(id);
	}
	catch (TechnicalException | DAOException e)
        {
            throw new LogicException(e);
        }
    }
    
    public static void addNewTest(HttpServletRequest request) throws LogicException
    {
	try(TestDAO dao = new TestDAO())
	{
	    dao.addNewTest(request);
	    addNewQuestions(request);
	}
	catch (TechnicalException | DAOException e)
        {
            throw new LogicException(e);
        }
    }
    
    public static void addNewQuestions(HttpServletRequest request) throws LogicException
    {
	try(TestDAO dao = new TestDAO())
	{
	    for(int i = 0; i < Integer.parseInt((String) request.getSession(true).getAttribute("inputnumber")); i++)
	    {
		Question question = new Question((String) request.getParameter("q" + (i + 1)));
		dao.addNewQuestion(question);
		addNewAnswers(request, i + 1);
	    }
	}
	catch (TechnicalException | DAOException e)
        {
            throw new LogicException(e);
        }
    }

    public static void addNewAnswers(HttpServletRequest request, int question) throws LogicException
    {
	try(TestDAO dao = new TestDAO())
	{
	    for(int j = 0; j < 5; j++)
	    {
		boolean b;
		if(request.getParameter("q" + question + "ch" + (j + 1)) != null)
		{
		    b = true;
		}
		else
		{
		    b = false;
		}
		Answer answer = new Answer((String) request.getParameter("q" + question + "a" + (j + 1)), b);
		dao.addNewAnswer(answer);
	    }
	}
	catch (TechnicalException | DAOException e)
        {
            throw new LogicException(e);
        }
    }

    public static void updateTest(int testid, HttpServletRequest request) throws LogicException
    {
	try(TestDAO dao = new TestDAO())
	{
	    Test test = getTest(testid);

	    List<Question> questionList = test.getList();
	    for(int i = 0; i < questionList.size(); i++)
	    {
		Question question = questionList.get(i);
		if(!question.getQuestionText().equals(request.getParameter("q" + (i + 1))))
		{
		    dao.updateQuestion(question.getId(), (String) request.getParameter("q" + (i + 1)));
		}
		
		List<Answer> answerList = question.getAnswerList();
		for(int j = 0; j < answerList.size(); j++)
		{
		    Answer answer = answerList.get(j);
		    boolean right;
		    if(request.getParameter("q" + (i + 1) + "ch" + (j + 1)) != null)
		    {
			right = true;
		    }
		    else
		    {
			right = false;
		    }
		    if(!answer.getAnswerText().equals(request.getParameter("q" + (i + 1) + "a" + (j + 1)))
			    || right != answer.isRight())
		    {
			dao.updateAnswer(answer.getId(), answer.getAnswerText(), right);
		    }
		}
	    }
	}
	catch (TechnicalException | DAOException e)
        {
            throw new LogicException(e);
        }
    }
    
    public static void deleteTest(int testid) throws LogicException
    {
	try(TestDAO dao = new TestDAO())
	{
	    Test test = getTest(testid);

	    List<Question> questionList = test.getList();
	    for(int i = 0; i < questionList.size(); i++)
	    {
		dao.deleteAnswers(questionList.get(i).getId());
	    }
	    dao.deleteQuestions(testid);
	    dao.deleteResults(testid);
	    dao.deleteTest(testid);
	}
	catch (TechnicalException | DAOException e)
	{
	    throw new LogicException(e);
	}
    }
}

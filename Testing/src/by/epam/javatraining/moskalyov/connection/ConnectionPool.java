package by.epam.javatraining.moskalyov.connection;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import javax.servlet.Servlet;

import org.apache.log4j.Logger;

import by.epam.javatraining.moskalyov.exception.TechnicalException;

/**
 * <p>
 * This class is thread-safe singleton that represents
 * connection pool with database of this application.
 * </p>
 * <p>
 * Call {@link ConnectionPool#getInstance()} method to get pool instance.
 * </p>
 * <p>
 * It uses {@code jdbc} driver to establish connections.
 * </p>
 * <p>
 * 20 connections are created by default.
 * </p>
 * <p>
 * To get a free connection use {@link ConnectionPool#getConnection(int)} method.
 * </p>
 */
public class ConnectionPool
{
    private static ConnectionPool instance;
    private static Lock lock = new ReentrantLock();
    private static final int TIME_WAIT = 1000;
    private static AtomicBoolean created = new AtomicBoolean(false);
    private static AtomicBoolean releasing = new AtomicBoolean(false);

    private static final int POOL_CAPACITY = 20;
    private ArrayBlockingQueue<Connection> connections;

    private static final String DB_CONFIGURATION_URL = "dbconf.properties";
    private static final String DB_URL_KEY_NAME = "url";
    private static final Logger LOG = Logger.getLogger(ConnectionPool.class);

    private ConnectionPool()
    {
        Properties properties = new Properties();
        String url;
        try
        {
            //properties.load(getClass().getResourceAsStream(DB_CONFIGURATION_URL));
            //url = properties.getProperty(DB_URL_KEY_NAME);
            DriverManager.registerDriver(new com.mysql.jdbc.Driver());
        }
        catch (SQLException e) // | IOException
        {
            throw new ExceptionInInitializerError(e);
        }
        connections = new ArrayBlockingQueue<>(POOL_CAPACITY);
        for (int i = 0; i < POOL_CAPACITY; i++)
        {
            try
            {
        	String url1="jdbc:mysql://localhost/testing?characterEncoding=utf8";
            	String user="root";
            	String password="";
                connections.add(DriverManager.getConnection(url1, user, password));
            }
            
            catch (SQLException e)
            {
                LOG.error("couldn't create connection", e);
            }
        }
        if (connections.size() == 0)
        {
            throw new ExceptionInInitializerError("problems initializing connection pool");
        }
    }

    public static ConnectionPool getInstance()
    {
        if (!created.get())
        {
            try
            {
                if (lock.tryLock(TIME_WAIT, TimeUnit.MILLISECONDS))
                {
                    if (!created.get())
                    {
                        try
                        {
                            instance = new ConnectionPool();
                            created.set(true);
                        }
                        finally
                        {
                            lock.unlock();
                        }
                    }
                }
            }
            catch (InterruptedException e)
            {
                LOG.error(e);
            }
        }
        return instance;
    }

    /**
     * Tries to give a free {@link Connection} to user. If there is no free {@code connection}, it waits
     * some time for a free {@code connection}.
     * <p>
     * After using a connection, release it using {@link ConnectionPool#releaseConnection(Connection)} method.
     * </p>
     *
     * @param waitTime time in seconds to wait for a free {@code connection}
     * @return a free {@code connection}
     * @throws TechnicalException if there is no free {@code connection} even after waiting,
     *                            pool is releasing at the moment
     *                            or thread is interrupted while waiting
     */
    public Connection getConnection(int waitTime) throws TechnicalException
    {
        if (releasing.get())
        {
            throw new TechnicalException("pool is releasing now, cannot take connection");
        }
        try
        {
            Connection connection = connections.poll(waitTime, TimeUnit.SECONDS);
            if (connection != null)
            {
                return connection;
            }
            else
            {
                throw new TechnicalException("waiting time for connection has passed");
            }
        }
        catch (InterruptedException e)
        {
            throw new TechnicalException(e);
        }
    }

    /**
     * Call this method to release a connection by returning it to pool.
     *
     * @param connection connection you want to release
     */
    public void releaseConnection(Connection connection)
    {
        connections.add(connection);
    }

    /**
     * Releases pool by closing all connections in it.
     * <p>
     * If problems occurred closing any connection, writes to
     * log but doesn't throw an exception.
     * </p>
     * <p>
     * Call this method at the end of application, e.g. in {@link Servlet#destroy()} method.
     * </p>
     */
    public void releasePool()
    {
        while (!connections.isEmpty())
        {
            Connection connection;
            if ((connection = connections.poll()) != null)
            {
                try
                {
                    connection.close();
                }
                catch (SQLException e)
                {
                    LOG.error("error releasing pool", e);
                }
            }
        }
    }

    public int getConnectionsQuantity()
    {
        return connections.size();
    }
}


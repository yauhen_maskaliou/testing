package by.epam.javatraining.moskalyov.command;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;

import by.epam.javatraining.moskalyov.exception.LogicException;
import by.epam.javatraining.moskalyov.logic.TestLogic;
import by.epam.javatraining.moskalyov.logic.UserLogic;

/**
 * Checks possibility of logging in and (if possible) log user in. If not, returns page with special message.
 */
public class LoginCommand implements Command
{
    public static final Logger LOG = Logger.getRootLogger();
    
    /**
     * Executes defined command: it does all necessary business logic, sets all attributes,
     * session attributes, etc. and returns address of response page.
     *
     * @param request request from client
     * @return string with response page relative address
     * @throws LogicException if any logic problems occur, such as incorrect user
     *                        data given through request parameters, invalid authorization
     *                        status and so on
     */
    @Override
    public String execute(HttpServletRequest request) throws LogicException
    {
	String login = request.getParameter("inputlogin");
	String password = request.getParameter("inputpassword");
	request.getSession(true).setAttribute("lang", request.getParameter("lang"));
	
	int id = UserLogic.studentExists(login, password);
	if(id != 0)
	{
	    List<String> topics = TestLogic.getAllTopics();
	    request.setAttribute("list", topics);
	    request.getSession(true).setAttribute("id", id);
	    request.getSession(true).setAttribute("login", login);
	    request.getSession(true).setAttribute("role", "student");
	    LOG.info("Student " + login + " logged in.");
	    return "/topics.jsp"; 
	}
	else
	{
	    id = UserLogic.tutorExists(login, password);
	    if (id != 0)
	    {
		List<String> topics = TestLogic.getAllTopics();
		request.setAttribute("list", topics);
		request.getSession(true).setAttribute("id", id);
		request.getSession(true).setAttribute("login", login);
		request.getSession(true).setAttribute("role", "tutor");
		LOG.info("Tutor " + login + " logged in.");
		return "/t_topics.jsp";
	    }
	}
	
	return "/incorrectinput.jsp";
    }
}

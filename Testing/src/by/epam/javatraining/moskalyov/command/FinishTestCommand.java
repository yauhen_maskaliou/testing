package by.epam.javatraining.moskalyov.command;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;

import by.epam.javatraining.moskalyov.exception.LogicException;
import by.epam.javatraining.moskalyov.logic.TestLogic;
import by.epam.javatraining.moskalyov.test.Test;

/**
 * Counts right answers, inserts new result into database and send result to student.
 */
public class FinishTestCommand implements Command
{
    public static final Logger LOG = Logger.getRootLogger();
    
    /**
     * Executes defined command: it does all necessary business logic, sets all attributes,
     * session attributes, etc. and returns address of response page.
     *
     * @param request request from client
     * @return string with response page relative address
     * @throws LogicException if any logic problems occur, such as incorrect user
     *                        data given through request parameters, invalid authorization
     *                        status and so on
     */
    @Override
    public String execute(HttpServletRequest request) throws LogicException
    {
	int testid = (int) request.getSession(true).getAttribute("testid");
	int studentid = (int) request.getSession(true).getAttribute("id");
	Test test = TestLogic.getTest(testid);

	String result = TestLogic.checkResults(test, request);
	TestLogic.addNewResult(testid, studentid, result);
	request.setAttribute("result", result);
	
	 LOG.info("Student " + request.getSession(true).getAttribute("login") + " passed test (" 
	 + testid + ") with result " + result + ".");
	
	return "/testresult.jsp";
    }
}

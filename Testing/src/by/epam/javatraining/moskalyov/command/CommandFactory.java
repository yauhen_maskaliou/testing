package by.epam.javatraining.moskalyov.command;

import javax.servlet.http.HttpServletRequest;

import by.epam.javatraining.moskalyov.exception.LogicException;

/**
 * Creates new {@link Command} object depending on it's string value.
 * <p>
 * Uses {@link CommandEnum command enumeration} for defining appropriate command.
 * </p>
 */
public class CommandFactory
{
    
    private static final String COMMAND_PARAMETER_NAME = "command";

    private CommandFactory()
    {
	
    }

    /**
     * Tries to define command by given string value.
     * <p>
     * Returns {@link Command} object according to {@code "command"} {@code parameter} value from client's {@code request}.
     * <br/>
     * If {@code parameter} value is {@code null} it returns {@link EmptyCommand} object.
     * </p>
     *
     * @param request request from client that contains parameter with name {@code "command"}
     * @return {@link by.epam.javatraining.moskalyov.command.Command} object from {@link by.epam.javatraining.moskalyov.command.CommandEnum}
     * @throws LogicException if incorrect request {@code "command"} parameter value is given
     * @see CommandEnum
     */
    public static Command defineCommand(HttpServletRequest request) throws LogicException
    {

        String command = request.getParameter(COMMAND_PARAMETER_NAME);
        if (command != null)
        {
            try
            {
                return CommandEnum.valueOf(command.toUpperCase()).getCommand();
            }
            catch (IllegalArgumentException e)
            {
                throw new LogicException("wrong command value", e);
            }
        }
        else
        {
            return CommandEnum.EMPTY.getCommand();
        }
    }
}


package by.epam.javatraining.moskalyov.command;

import javax.servlet.http.HttpServletRequest;


/**
 * <p>
 * This {@code enumeration} represents list of commands that can be executed
 * in this application context.
 * </p>
 * <p>Selecting each item of this {@code enum}
 * creates a new appropriate {@link Command} object
 * that can be returned by calling {@link CommandEnum#getCommand()} method.
 * Than you can call {@link Command#execute(HttpServletRequest)} method
 * on this object to perform defined command.
 * </p>
 * <p>
 * For each element of this {@code enum} see appropriate {@link Command} object docs.
 * </p>
 */
public enum CommandEnum
{
    /**
     * @see EmptyCommand
     */
    EMPTY
    {
        {
            command = new EmptyCommand();
        }
    },
    
    /**
     * @see LoginCommand
     */
    LOGIN
            {
                {
                    command = new LoginCommand();
                }
            },

    /**
    * @see ShowTestsOnTopicCommand
    */
    SHOWTESTSONTOPIC
    {
	{
	    command = new ShowTestsOnTopicCommand();
	}
    },
    
    /**
     * @see ShowTestCommand
     */
    SHOWTEST
    {
	
	{
	    command = new ShowTestCommand();
	}
    },
    
    /**
     * @see ShowRegisterFormCommand
     */
    SHOWREGISTERFORM
    {
	{
	    command = new ShowRegisterFormCommand();
	}
    },
    
    /**
     * @see RegisterCommand
     */
    REGISTER
    {
	{
	    command = new RegisterCommand();
	}
    },
    
    /**
     * @see ShowStartPageCommand
     */
    SHOWSTARTPAGE
    {
	{
	    command = new ShowStartPageCommand();
	}
    },
    
    /**
     * @see FinishTestCommand
     */
    FINISHTEST
    {
	{
	    command = new FinishTestCommand();
	}
    },
    
    /**
     * @see ShowTopicsCommand
     */
    SHOWTOPICS
    {
	{
	    command = new ShowTopicsCommand();
	}
    },
    
    /**
     * @see ShowStudentResultsCommand
     */
    SHOWSTUDENTRESULTS
    {
	{
	    command = new ShowStudentResultsCommand();
	}
    },
    
    /**
     * @see ShowTestStatsCommand
     */
    SHOWTESTSTATS
    {
	{
	    command = new ShowTestStatsCommand();
	}
    },
    
    /**
     * @see AddTestCommand
     */
    ADDTEST
    {
	{
	    command = new AddTestCommand();
	}
    },
    
    /**
     * @see ShowAddFormCommand
     */
    SHOWADDFORM
    {
	{
	    command = new ShowAddFormCommand();
	}
    },
    
    /**
     * @see FinishAddingCommand
     */
    FINISHADDING
    {
	{
	    command = new FinishAddingCommand();
	}
    },
    
    /**
     * @see EditTestCommand
     */
    EDITTEST
    {
	{
	    command = new EditTestCommand();
	}
    },
    
    /**
     * @see FinishEditingCommand
     */
    FINISHEDITING
    {
	{
	    command = new FinishEditingCommand();
	}
    },

    /**
     * @see DeleteTestCommand
     */
    DELETETEST
    {
	{
	    command = new DeleteTestCommand();
	}
    },
    
    /**
     * @see LogoutCommand
     */
    LOGOUT
    {
	{
	    command = new LogoutCommand();
	}
    }
    ;
    protected Command command;

    /**
     * <p>
     * Get the current command. Is called by appropriate {@code enum} element.
     * E.g., if you need login command, you're doing:
     * </p>
     * <p>
     * <code>
     * CommandEnum definedCommand = CommandEnum.LOGIN;
     * <br/>
     * String responseAddress = definedCommand.getCommand().execute(request);
     * </code>
     * </p>
     *
     * @return current {@link Command} object.
     */
    public Command getCommand()
    {
        return command;
    }
}

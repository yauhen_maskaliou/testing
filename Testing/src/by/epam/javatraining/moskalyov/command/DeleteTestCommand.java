package by.epam.javatraining.moskalyov.command;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;

import by.epam.javatraining.moskalyov.exception.LogicException;
import by.epam.javatraining.moskalyov.logic.TestLogic;

/**
 * Deletes test selected by tutor.
 */
public class DeleteTestCommand implements Command
{
    public static final Logger LOG = Logger.getRootLogger();
    
    /**
     * Executes defined command: it does all necessary business logic, sets all attributes,
     * session attributes, etc. and returns address of response page.
     * 
     * @param request request from client
     * @return string with response page relative address
     * @throws LogicException if any logic problems occur, such as incorrect user
     *                        data given through request parameters, invalid authorization
     *                        status and so on
     */
    @Override
    public String execute(HttpServletRequest request) throws LogicException
    {
	int testid = (int) request.getSession(true).getAttribute("testid");
	TestLogic.deleteTest(testid);
	
	LOG.info("Tutor " + request.getSession(true).getAttribute("login") + " has deleted test "
	+ testid + ".");
	
	return "/operationsucceed.jsp";
    }
}

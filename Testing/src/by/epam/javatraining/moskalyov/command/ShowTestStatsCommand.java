package by.epam.javatraining.moskalyov.command;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import by.epam.javatraining.moskalyov.exception.LogicException;
import by.epam.javatraining.moskalyov.logic.TestLogic;
import by.epam.javatraining.moskalyov.test.TestResult;

/**
 * Returns page with stats on selected test.
 */
public class ShowTestStatsCommand implements Command
{
    /**
     * Executes defined command: it does all necessary business logic, sets all attributes,
     * session attributes, etc. and returns address of response page.
     *
     * @param request request from client
     * @return string with response page relative address
     * @throws LogicException if any logic problems occur, such as incorrect user
     *                        data given through request parameters, invalid authorization
     *                        status and so on
     */
    @Override
    public String execute(HttpServletRequest request) throws LogicException
    {
	int testid = (int) request.getSession(true).getAttribute("testid");
	List<TestResult> results = TestLogic.getTestStats(testid);
	
	request.setAttribute("results", results);
	request.setAttribute("title", TestLogic.getTest(testid).getTitle());
	return "/teststats.jsp";
    }
}

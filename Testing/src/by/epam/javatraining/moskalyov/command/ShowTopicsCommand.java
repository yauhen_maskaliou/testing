package by.epam.javatraining.moskalyov.command;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import by.epam.javatraining.moskalyov.exception.LogicException;
import by.epam.javatraining.moskalyov.logic.TestLogic;

/**
 * Returns page with all available topics.
 */
public class ShowTopicsCommand implements Command
{
    /**
     * Executes defined command: it does all necessary business logic, sets all attributes,
     * session attributes, etc. and returns address of response page.
     *
     * @param request request from client
     * @return string with response page relative address
     * @throws LogicException if any logic problems occur, such as incorrect user
     *                        data given through request parameters, invalid authorization
     *                        status and so on
     */
    @Override
    public String execute(HttpServletRequest request) throws LogicException
    {
	List<String> topics = TestLogic.getAllTopics();
	request.setAttribute("list", topics);
	if(request.getSession().getAttribute("role").equals("student"))
	{
	    return "/topics.jsp";
	}
	else
	{
	    return "/t_topics.jsp";
	}
    }
}

package by.epam.javatraining.moskalyov.command;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;

import by.epam.javatraining.moskalyov.exception.LogicException;
import by.epam.javatraining.moskalyov.logic.TestLogic;
import by.epam.javatraining.moskalyov.logic.UserLogic;

/**
 * Registers new user if login is free. If not, returns page with special message.
 */
public class RegisterCommand implements Command
{
    public static final Logger LOG = Logger.getRootLogger();
    
    /**
     * Executes defined command: it does all necessary business logic, sets all attributes,
     * session attributes, etc. and returns address of response page.
     *
     * @param request request from client
     * @return string with response page relative address
     * @throws LogicException if any logic problems occur, such as incorrect user
     *                        data given through request parameters, invalid authorization
     *                        status and so on
     */
    @Override
    public String execute(HttpServletRequest request) throws LogicException
    {
	String login=request.getParameter("inputlogin");
	String password=request.getParameter("inputpassword");
	String name=request.getParameter("inputname");
	String surname=request.getParameter("inputsurname");
	
	if(UserLogic.isLoginFree(login))
	{
	    UserLogic.registerNewUser(login, password, name, surname);
	    
	    int id = UserLogic.studentExists(login, password);
	    
	    List<String> topics = TestLogic.getAllTopics();
	    request.setAttribute("list", topics);
	    request.getSession(true).setAttribute("id", id);
	    request.getSession(true).setAttribute("role", "student");
	    request.getSession(true).setAttribute("login", login);
	    
	    LOG.info("Student " + login + " has registered.");
	    
	    return "/topics.jsp"; 
	}
	return "/loginnotfree.jsp";
    }
}

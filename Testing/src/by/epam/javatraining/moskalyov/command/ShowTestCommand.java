package by.epam.javatraining.moskalyov.command;

import javax.servlet.http.HttpServletRequest;

import by.epam.javatraining.moskalyov.exception.LogicException;
import by.epam.javatraining.moskalyov.logic.TestLogic;

/**
 * Returns page with test, including questions and answer variants.
 */
public class ShowTestCommand implements Command
{
    /**
     * Executes defined command: it does all necessary business logic, sets all attributes,
     * session attributes, etc. and returns address of response page.
     *
     * @param request request from client
     * @return string with response page relative address
     * @throws LogicException if any logic problems occur, such as incorrect user
     *                        data given through request parameters, invalid authorization
     *                        status and so on
     */
    public String execute(HttpServletRequest request) throws LogicException
    {
	int testid = Integer.parseInt(request.getParameter("testid"));
	request.setAttribute("list", TestLogic.getTest(testid));
	request.getSession(true).setAttribute("testid", testid);
	
	if(request.getSession().getAttribute("role").equals("student"))
	{
	    return "/test.jsp";
	}
	else
	{
	    return "/t_test.jsp";
	}
    }
}

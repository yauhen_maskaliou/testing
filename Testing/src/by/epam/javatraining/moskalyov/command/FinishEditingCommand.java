package by.epam.javatraining.moskalyov.command;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;

import by.epam.javatraining.moskalyov.exception.LogicException;
import by.epam.javatraining.moskalyov.logic.TestLogic;

/**
 * Updates test edited by tutor in database.
 */
public class FinishEditingCommand implements Command
{
    public static final Logger LOG = Logger.getRootLogger();
    
    /**
     * Executes defined command: it does all necessary business logic, sets all attributes,
     * session attributes, etc. and returns address of response page.
     *
     * @param request request from client
     * @return string with response page relative address
     * @throws LogicException if any logic problems occur, such as incorrect user
     *                        data given through request parameters, invalid authorization
     *                        status and so on
     */
    @Override
    public String execute(HttpServletRequest request) throws LogicException
    {
	int testid = (int) request.getSession(true).getAttribute("testid");
	TestLogic.updateTest(testid, request);
	
	LOG.info("Tutor " + request.getSession(true).getAttribute("login") 
		+ " has edited test(" + testid + ").");
	
	return "/operationsucceed.jsp";
    }
}

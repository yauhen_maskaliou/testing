package by.epam.javatraining.moskalyov.command;

import java.util.Enumeration;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;

import by.epam.javatraining.moskalyov.exception.LogicException;

/**
 * Logs user out.
 */
public class LogoutCommand implements Command
{
    public static final Logger LOG = Logger.getRootLogger();
    
    /**
     * Executes defined command: it does all necessary business logic, sets all attributes,
     * session attributes, etc. and returns address of response page.
     *
     * @param request request from client
     * @return string with response page relative address
     * @throws LogicException if any logic problems occur, such as incorrect user
     *                        data given through request parameters, invalid authorization
     *                        status and so on
     */
    @Override
    public String execute(HttpServletRequest request) throws LogicException
    {
	LOG.info("User " + request.getSession(true).getAttribute("login") + " logged out.");
	
	Enumeration<String> e = request.getSession(true).getAttributeNames();
	while(e.hasMoreElements())
	{
	    request.getSession(true).removeAttribute(e.nextElement());
	}
	
	return "/index.jsp";
    }
}

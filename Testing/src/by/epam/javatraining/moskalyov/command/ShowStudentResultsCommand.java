package by.epam.javatraining.moskalyov.command;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import by.epam.javatraining.moskalyov.exception.LogicException;
import by.epam.javatraining.moskalyov.logic.TestLogic;
import by.epam.javatraining.moskalyov.test.TestResult;

/**
 * Returns page with all student results.
 */
public class ShowStudentResultsCommand implements Command
{
    /**
     * Executes defined command: it does all necessary business logic, sets all attributes,
     * session attributes, etc. and returns address of response page.
     *
     * @param request request from client
     * @return string with response page relative address
     * @throws LogicException if any logic problems occur, such as incorrect user
     *                        data given through request parameters, invalid authorization
     *                        status and so on
     */
    @Override
    public String execute(HttpServletRequest request) throws LogicException
    {
	int studentid = (int) request.getSession(true).getAttribute("id");
	List<TestResult> results = TestLogic.getStudentResults(studentid);
	
	request.setAttribute("results", results);
	
	return "/studentresults.jsp";
    }
}

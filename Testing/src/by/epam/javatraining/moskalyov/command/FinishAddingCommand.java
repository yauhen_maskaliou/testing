package by.epam.javatraining.moskalyov.command;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;

import by.epam.javatraining.moskalyov.exception.LogicException;
import by.epam.javatraining.moskalyov.logic.TestLogic;

/**
 * Adds new test created by tutor to database.
 */
public class FinishAddingCommand implements Command
{
    public static final Logger LOG = Logger.getRootLogger();
    
    /**
     * Executes defined command: it does all necessary business logic, sets all attributes,
     * session attributes, etc. and returns address of response page.
     *
     * @param request request from client
     * @return string with response page relative address
     * @throws LogicException if any logic problems occur, such as incorrect user
     *                        data given through request parameters, invalid authorization
     *                        status and so on
     */
    @Override
    public String execute(HttpServletRequest request) throws LogicException
    {
	TestLogic.addNewTest(request);
	
	LOG.info("Tutor " + request.getSession(true).getAttribute("login") + " added new test.");
	
	return "/operationsucceed.jsp";
    }
}

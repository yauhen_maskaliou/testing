package by.epam.javatraining.moskalyov.command;

import javax.servlet.http.HttpServletRequest;

import by.epam.javatraining.moskalyov.exception.LogicException;

/**
 * Defines {@code interface} for any command.
 * <br/>
 * Command is some entity that means doing certain actions,
 * e.g. moving forward to some page or adding information to database.
 * <p>
 * This {@code interface} consists of only one {@link Command#execute(HttpServletRequest) method} that means doing the command.
 * It does all necessary business logic, sets all attributes,
 * session attributes, etc. and returns address of response page.
 * </p>
 */
public interface Command
{
    /**
     * Executes defined command: it does all necessary business logic, sets all attributes,
     * session attributes, etc. and returns address of response page.
     *
     * @param request request from client
     * @return string with response page relative address
     * @throws LogicException if any logic problems occur, such as incorrect user
     *                        data given through request parameters, invalid authorization
     *                        status and so on
     */
    public String execute(HttpServletRequest request) throws LogicException;
}

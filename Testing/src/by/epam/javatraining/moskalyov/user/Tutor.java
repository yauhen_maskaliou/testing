package by.epam.javatraining.moskalyov.user;

import java.util.List;

import by.epam.javatraining.moskalyov.test.Test;

public class Tutor extends User
{
    public Tutor(String login, String password, String name, String surname,
	    List<Test> addedTests)
    {
	super(login, password, name, surname);
    }
}
